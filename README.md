# Breakout

## instalation

Open a command prompt or powershell.

`cd` to a folder of your choice.

now run

`git clone https://gitlab.com/school4/2019-2020/software-development-v/breakout.git`

and go into the newly created folder

`cd breakout`

### VS code

still in your prompt, type `code .` and hit enter, this will open code in the current folder.

Now first install the liveserver extension if you haven't already.

If you have installed live server then go to the index.html file and right click in it. Click open with live server and it should work.

### atom

I don't have atom but i guess you can do the same as in VS code.

## files

### index.html

Simple html file where we import all necessary things.

### breakout.png

Image containing all the assets needed to give the game textures.

### breakout.json

Json file containing all the mapping information for the different assets that are in the breakout.png file

### game.js

Here is all the code that actually runs the game and displays it in your browser. More info can be found in the file itself.
