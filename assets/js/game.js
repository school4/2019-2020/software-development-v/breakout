const Breakout = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:
        /**
         * Breakout class 
         * here are all the games variables
         */
        class Breakout {
            constructor () {
                Phaser.Scene.call(this, { key: 'breakout' });

                this.bricks;
                this.paddle;
                this.ball;
                this.score = 0;
            }

            /**
             * get score in text format
             * @return {string}
             */
            get scoreText() {
                return `Score: ${this.score}`;
            }
        },

    /** 
     * preload the games assets
     * @return {void}
     */
    preload: function () {
        this.load.atlas('assets', '/assets/images/breakout.png', '/assets/json/breakout.json');
    },

    /**
     * creates game world and it's assets
     * @return {void}
     */
    create: function () {
        //  Enable world bounds, but disable the floor
        this.physics.world.setBoundsCollision(true, true, true, false);

        //  Create the bricks in a 10x6 grid
        this.bricks = this.physics.add.staticGroup({
            key: 'assets', frame: ['blue1', 'red1', 'green1', 'yellow1', 'silver1', 'purple1'],
            frameQuantity: 10,
            gridAlign: { width: 10, height: 6, cellWidth: 64, cellHeight: 32, x: 112, y: 100 }
        });

        this.ball = this.physics.add.image(400, 500, 'assets', 'ball1').setCollideWorldBounds(true).setBounce(1);
        this.ball.setData('onPaddle', true);

        this.paddle = this.physics.add.image(400, 550, 'assets', 'paddle1').setImmovable();

        //  Our colliders
        this.physics.add.collider(this.ball, this.bricks, this.hitBrick, null, this);
        this.physics.add.collider(this.ball, this.paddle, this.hitPaddle, null, this);

        this.scoreField = this.add.text(20, 10, this.scoreText, {
            font: '20px Arial',
            fill: '#fff',
            align: 'center'
        });

        //  Input events
        // paddle follows mouse
        this.input.on('pointermove', function (pointer) {

            //  Keep the paddle within the game
            this.paddle.x = Phaser.Math.Clamp(pointer.x, 52, 748);

            if (this.ball.getData('onPaddle')) {
                this.ball.x = this.paddle.x;
            }

        }, this);

        // start game when ball is on paddle when mouse is clicked
        this.input.on('pointerup', function (pointer) {

            if (this.ball.getData('onPaddle')) {
                this.ball.setVelocity(-75, -300);
                this.ball.setData('onPaddle', false);
            }

        }, this);
    },

    /**
     * Handles the ball hitting a brick
     * @param {ball} ball ball object
     * @param {brick} brick brick that has just been hit
     * @return {void}
     */
    hitBrick: function (ball, brick) {
        brick.disableBody(true, true);

        this.score += 100;

        if (this.bricks.countActive() === 0) {
            this.resetLevel();
        }
    },

    /**
     * Reset the balls position
     * @return {void}
     */
    resetBall: function () {
        this.ball.setVelocity(0);
        this.ball.setPosition(this.paddle.x, 500);
        this.ball.setData('onPaddle', true);
    },

    /**
     * Reset the game
     * @return {void}
     */
    resetLevel: function () {
        this.resetBall();

        this.score = 0;

        this.bricks.children.each(function (brick) {

            brick.enableBody(false, 0, 0, true, true);

        });
    },

    /**
     * Handles what the ball has to do when it hits your paddle
     * @return {void}
     */
    hitPaddle: function (ball, paddle) {
        let diff = 0;

        if (ball.x < paddle.x) {
            //  Ball is on the left-hand side of the paddle
            diff = paddle.x - ball.x;
            ball.setVelocityX(-10 * diff);
        }
        else if (ball.x > paddle.x) {
            //  Ball is on the right-hand side of the paddle
            diff = ball.x - paddle.x;
            ball.setVelocityX(10 * diff);
        }
        else {
            //  Ball is perfectly in the middle
            //  Add a little random X to stop it bouncing straight up!
            ball.setVelocityX(2 + Math.random() * 8);
        }
    },

    /**
     * update the game
     * @return {void}
     */
    update: function () {
        this.scoreField.text = this.scoreText;

        // on every update check ball y position
        // this to check if it hasn't hit the floor yet
        if (this.ball.y > 600) {
            this.score -= 200;
            this.resetBall();
        }
    }

});

// phaser.js config
const config = {
    type: Phaser.AUTO,
    width: 800, // width of canvas
    height: 600, // height of canvas
    parent: 'breakout', // DOM element id into which the canvas will be injected
    scene: [Breakout], // Scene to play
    physics: {
        default: 'arcade'
    } // game physics
};

// start new game
const game = new Phaser.Game(config);